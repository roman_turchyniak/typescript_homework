import { fight } from "../fight";
import { showModal } from "./modal";

export interface IFighter {
  _id: number;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source?: string;
  loser?: boolean;
  inBlock?: boolean;
  CritAllow?: boolean;
  MaxHp?: number;
  healthPresentage?: number;
  isCritical?: boolean
}

export function showWinnerModal(fighter: IFighter) {
  // call showModal function 
  showModal({title: `${fighter.name} WIN`})
}
