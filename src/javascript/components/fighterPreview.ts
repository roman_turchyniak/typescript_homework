import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  let imgElement = createFighterImage(fighter);

  let infoBlock = createElement({
    tagName: 'ul',
    className: 'fighter-preview__el',
  });

  let fighterName = createElement({
    tagName: 'li',
    className: 'fighter-preview__el',
  });
  fighterName.innerHTML = `Name: ${fighter.name}`;

  let health = createElement({
    tagName: 'li',
    className: 'fighter-preview__el',
  });
  health.innerHTML = `HP: ${fighter.health}`;

  let atk = createElement({
    tagName: 'li',
    className: 'fighter-preview__el',
  });
  atk.innerHTML = `Attack Power: ${fighter.attack}`;

  let def = createElement({
    tagName: 'li',
    className: 'fighter-preview__el',
  });
  def.innerHTML = `Defence Value: ${fighter.defense}`;

  infoBlock.appendChild(fighterName);
  infoBlock.appendChild(health);
  infoBlock.appendChild(atk);
  infoBlock.appendChild(def);
  fighterElement.appendChild(imgElement);
  fighterElement.appendChild(infoBlock);
  
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
